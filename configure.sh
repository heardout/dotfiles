#!/bin/bash

# Update package lists
echo "Updating package lists..."
sudo apt update

# Upgrade and autoremove packages
echo ''
echo "Upgrading and auto removing packages..."
sudo apt upgrade -y && sudo apt autoremove -y

# Install zsh
which zsh > /dev/null 2>&1

if [[ $? -eq 0 ]] ; then
    echo ''
    echo "Skipping zsh, already installed..."
else
    echo "Installing zsh..."
    echo ''

    sudo apt install zsh -y
fi

# Install oh-my-zsh
if [ -d ~/.oh-my-zsh/ ] ; then
    echo ''
    echo "Skipping oh-my-zsh, already installed..."

    read -p "Would you like to update oh-my-zsh?" -n 1 -r
    echo ''

    if [[ $REPLY =~ ^[Yy]$ ]] ; then
        cd ~/.oh-my-zsh && git pull

        if [[ $? -eq 0 ]] ; then
            echo "Update complete..." && cd
        else
            echo "Update failed..." >&2 cd
        fi
    fi
else
    echo "Installing oh-my-zsh..."
    echo ''

    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
fi

# Enable autocorrection
echo ''
echo "Enabling autocorrection..."

sed -i 's/# ENABLE_CORRECTION="true"/ENABLE_CORRECTION="true"/g' ~/.zshrc

# Install oh-my-zsh plugins
echo ''
echo "Installing oh-my-zsh plugins..."
echo ''

# Install zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

# Install zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# Install zsh-nvm
git clone https://github.com/lukechilds/zsh-nvm ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-nvm

# Install personal dotfiles
echo ''
read -p "Use personal dotfiles? y/n" -n 1 -r
echo

if [[ $REPLY =~ ^[Yy]$ ]] ; then
  echo ''
	echo "Pulling down personal dotfiles..."

  git clone https://thomaszaleski@bitbucket.org/heardout/dotfiles.git ~/.dotfiles

	echo ''
	cd $HOME/.dotfiles && echo "switched to .dotfiles directory..."

	echo ''
	echo "Now configuring symlinks..." && $HOME/.dotfiles/script/bootstrap

  if [[ $? -eq 0 ]] ; then
      echo "Successfully configured environment with personal dotfiles..."
  else
      echo "Failed configuring environment with personal dotfiles..." >&2
  fi
else
	echo ''
	echo "Setting theme to..."

  sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="agnoster"/g' ~/.zshrc

  echo ''
  echo "Enabling plugins..."

  sed -i 's/plugins=(git)/plugins=(git zsh-nvm zsh-autosuggestions zsh-syntax-highlighting)/g' ~/.zshrc
fi

# Set default shell to zsh
echo ''
read -p "Do you want to change your default shell? y/n" -n 1 -r
echo ''

if [[ $REPLY =~ ^[Yy]$ ]] ; then
	echo "Now setting default shell..."

  chsh -s $(which zsh)

  if [[ $? -eq 0 ]] ; then
      echo "Successfully set your default shell to zsh..."
  else
      echo "Default shell not set successfully..." >&2
  fi
else
    echo "You chose not to set your default shell to zsh. Exiting now..."
fi

echo ''
echo '	WSL terminal configured!'
